include_recipe "apt"
package "ruby"

git_client 'default' do
    action :install
end

git "/usr/local/cats" do
  repository node["cat"]["source"]["repo"]["uri"]
  tag node["cat"]["source"]["repo"]["tag"]
  action :sync
end

bash 'install_gems' do
    cwd node["cat"]["install"]["path"]
    code <<-EOH
      bundle install
      EOH
    not_if { ::File.exist?(node["cat"]["install"]["path"]) }
end


