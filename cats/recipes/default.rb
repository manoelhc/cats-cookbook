#
# Cookbook:: cat
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.


include_recipe "cat::_install"
include_recipe "cat::_config"
include_recipe "cat::_run"