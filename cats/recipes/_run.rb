bash 'run_cats' do
    cwd node["cat"]["install"]["path"]
    code <<-EOH
      bundle exec puma
    EOH
    not_if { ::File.exist?(node["cat"]["install"]["path"]) }
end