default["cat"]["source"]["repo"]["uri"] = "https://github.com/Streetbees/cats.git"
default["cat"]["source"]["repo"]["tag"] = "1.0.0"

default["cat"]["install"]["path"] = "/usr/local/cats"

default["cat"]["env"]["port"] = "8000"
default["cat"]["env"]["web_concurrency"] = "1"
default["cat"]["env"]["max_threads"] = "1"
default["cat"]["env"]["rack_env"] = "production"